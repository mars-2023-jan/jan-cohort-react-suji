import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css'
import { Button } from "react-bootstrap";


function Hooks(){
    const[color, setcolor]= useState("Brown")
    const[count, setcount] = useState(0)
    // const[car,setcar]=useState({
    //     make:"Ford",
    //     model:"Mustang",
    //     year :"1999",
    //     color:"white"
    // })

    const navigate =useNavigate()

    function handleClick(){
        navigate('/')
    }

    useEffect(()=>{
        setTimeout(()=>{
            setcount(count+1)
        },1000)
        // console.log('Use effect called....')
    },color)   // can also use [] empty array to call useEffect just once(to stop the loop)

    return(
        <div>

            <Button>BS Button</Button>
            {/* <h2>My Fav color is: {color}</h2>
            <button type="button" onClick={()=> setcolor("white")} > Change color</button> <br></br>
            <h3>My{car.make} car build in {car.year} is {car.color} color</h3>
            <button type="button" onClick={()=> setcar(mycar =>{
                return{...mycar,make:"hyundai"}})}> Update Car </button> */}
        </div>
    )
}

export default Hooks;

//states on function component