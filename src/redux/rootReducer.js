import { combineReducers } from "redux";
import counterReducer from "./counterReducer";
import colorReducer from "./colorReducer";
import tableDataReducer from "./tableDataReducer"

export const rootReducer = combineReducers({
    count: counterReducer,
    favColor : colorReducer,
    tableData : tableDataReducer
})