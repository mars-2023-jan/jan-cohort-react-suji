import React, { useReducer, useState} from 'react';
import '/node_modules/bootstrap/dist/css/bootstrap.min.css'

const ACTIONS = {
    INCREMENT : 'increment',
    DECREMENT : 'decrement',
    INPUTCHANGE : 'inputchange',
    TOGGLECOLOR : 'togglecolor'
}

const reducer = (state,action)=>{
    
    switch(action.type){
        case ACTIONS.INCREMENT :
            return {...state, counter:state.counter+1};
        case ACTIONS.DECREMENT:
            return {...state, counter:state.counter-1};
        case ACTIONS.INPUTCHANGE:
            return {...state, inputVal:action.payload};
        case ACTIONS.TOGGLECOLOR:
            return{...state, color: !state.color};
        default :
            return state;
    }
}

function Reducer(props) {
    // const [inputVal, setInputVal]= useState('')
    // const [counter, setCounter] = useState(0)
    const [state, dispatch] = useReducer(reducer,{counter:0, inputVal:'', color:false})
    // const [color, setColor] = useState(false)


    return (
        <div className="form-group" style={{color:state.color? 'Red':'black'}}>
            <input className="form-control"
            type = 'text'
            placeholder="EnterText"
            onChange={(e)=>dispatch({type:ACTIONS.INPUTCHANGE, payload:e.target.value})}
            />

            <p>{state.inputVal}</p>
            <p>{state.counter}</p>
            <button 
            type= 'button' 
            className='btn-btn-info'
            onClick={()=>dispatch({type:ACTIONS.INCREMENT })} 
            >+</button>   &nbsp;&nbsp;
            <button 
            type= 'button' 
            className='btn-btn-info'
            onClick={()=>dispatch({type:ACTIONS.DECREMENT })}
            >-</button><br></br>

            <button 
            type= 'button' 
            className='btn-btn-dark'
            onClick={()=>dispatch({type:ACTIONS.TOGGLECOLOR })}>
            Toggle Color </button>
            
        </div>
    );
}

export default Reducer;

//Action: is an object with mandatory type property which will be passed to the reducer
//Reducer: Is a pure javascript function which will update the state
//dispatch : which Send action to reducer
//State : which holds all your component data