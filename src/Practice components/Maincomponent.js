import React, { useState } from 'react';
import Child from './Child';
function Maincomponent(props) {
    const [data,setdata]= useState("")
    
    const childtomain = (childdata)=>{
        setdata(childdata)
    }
    return (
        <div>
            { data }
            <div>
                <Child childtomain = {childtomain} />
            </div>
        </div>
    );
}

export default Maincomponent;