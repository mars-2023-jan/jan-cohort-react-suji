import React from "react";
import { render, screen} from '@testing-library/react'

import Demo from "./demo";

describe('Testing demo component', ()=>{
test('h1 element render in DOM', ()=>{
    render(<Demo />)
    screen.debug();
    expect(screen.getByText(/Hello/)).toBeInTheDocument();
    expect(screen.getByTestId("testid")).toBeInTheDocument();
})

test('list contains 5 items', ()=>{
    render (<Demo />)
    const listElement= screen.getByRole('list')
    const listItems= screen.getAllByRole('listitem')

    expect(listElement).toBeInTheDocument();
    expect(listItems.length).toBe(5)
})
})