import "./App.css";
// import Home from "./redux/Home";
import RouteForm from "./LoginForm/RouteForm";
import { configure } from "@testing-library/react";
import { configureStore } from "@reduxjs/toolkit";
import { Provider } from "react-redux";
import counterReducer from "./redux/counterReducer";
import myLogger from "./redux/myLogger";
import capAtTen from "./redux/capAtTen";
import colorReducer from "./redux/colorReducer";
import { rootReducer } from "./redux/rootReducer";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import Home from "./redux/Home";
import EmpDetails from "./Crud/EmpDetails";
import ProdDetails from "./ProdCrud/ProdDetails";

const reduxDevTools = composeWithDevTools();

function App() {
  console.log("Creating Store");
  const store = configureStore({
    reducer: rootReducer,
    middleware: [myLogger, capAtTen, thunk],
    devTools: reduxDevTools,
  });

  return (
    <div>
      <h1>React App</h1>
      <Provider store={store}>
        <Home />
      </Provider>
    </div>
  );
}

export default App;
