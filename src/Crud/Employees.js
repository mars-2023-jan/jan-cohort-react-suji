const Employees = [
  {
    id: 1,
    name: "Sam",
    age: 27,
  },
  {
    id: 2,
    name: "Anna",
    age: 30,
  },
  {
    id: 3,
    name: "Emma",
    age: 28,
  },
];

export default Employees;
