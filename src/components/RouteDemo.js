import React from "react";
import { BrowserRouter, Link, Routes, Route } from "react-router-dom";
import Header from "./Header";
import Footer from "./Footer";
import Error from "../Error";
import Hobbies from "../CheckHobbies/Hobbies";
import Expense from "../BudgetForm/Expense";
import Parent from "../Practice components/Parent";

function RouteDemo(props) {
  return (
    <div>
      <h1>Route Demo</h1>
      <BrowserRouter>
        <nav>
          <ul>
            <li>
              <Link to="/expense">Expense</Link>
            </li>
            <li>
              <Link to="/hobbies">Hobbies</Link>
            </li>
            <li>
              <Link to="/footer">Footer</Link>
            </li>
            <li>
              <Link to="/dynamic">Dynamic</Link>
            </li>
          </ul>
        </nav>
        <Routes>
          <Route path="/" element={<Header />} />
          <Route path="/expense" element={<Expense />} />
          <Route path="/hobbies" element={<Hobbies />} />
          <Route path="/footer" element={<Footer />} />
          <Route path="/dynamic" element={<Parent />} />
          <Route path="*" element={<Error />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default RouteDemo;
