import React, { Component } from 'react';

class Expense extends Component {
    constructor(props){
        super(props);
        this.state={                        //set initial state
          totalbudget:"",
            balance:"",
            childcare:"",
            insurance:"",
            mortgage:"",
            grocery:""
        }
     this.updateBalance = this.updateBalance.bind(this);  //Binding this keyword
    }

    updateBalance(e){                                     
      this.setState({[e.target.name]: e.target.value}, ()=> {      
        this.setState({balance: this.state.totalbudget - this.state.childcare - this.state.insurance- 
            this.state.mortgage- this.state.grocery});
        });
     }


render(){
    return (

        <div class="exp-container">
          <form>
            <label for="totalmonthlybudget"> Total Monthly Budget: </label>
            <input
              type="number"
              id="monthlybudget"
              name="totalbudget"
              value={this.state.totalbudget} 
              onChange={this.updateBalance}
            ></input>
            <label for="monthlybalance"> Monthly Balance: </label>
            <input value={this.state.balance} type="number" id="balance" name="balance"></input>
          </form>
    
          <h2>Expenses</h2>
    
          <form>
            <label for="Childcare"> Child Care : </label>
            <input
              type="number"
              id="childcare"
              name="childcare"
              value={this.state.childcare} 
              onChange={this.updateBalance}
            ></input>
            <br></br>
            <br></br>
            <label for="insurance"> Insurance : </label>
            <input
              type="number"
              id="insurance"
              name="insurance"
              value={this.state.insurance} 
              onChange={this.updateBalance}
            ></input>
            <br></br>
            <br></br>
            <label for="mortgage"> Mortgage : </label>
            <input
              type="number"
              id="mortgage"
              name="mortgage" 
              value={this.state.mortgage} 
              onChange={this.updateBalance}
            ></input>
            <br></br>
            <br></br>
            <label for="grocery"> Grocery : </label>
            <input
              type="number"
              id="grocery"
              name="grocery"
              value={this.state.grocery} 
              onChange={this.updateBalance}
            ></input>
            <br></br>
            <br></br>
          </form>
        </div>
      );
    }
}

export default Expense;