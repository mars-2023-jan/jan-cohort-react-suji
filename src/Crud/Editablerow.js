import React from 'react';
import { FaSave } from 'react-icons/fa';

const Editablerow = ({editEmpData, handleEditChange}) => {
    return (
        <tr>
            <td>
                <input
                type = 'text'
                required= 'required'
                name='name'
                value ={editEmpData.name}
                placeholder='Enter Name'
                onChange={handleEditChange}>
                </input>
            </td>
            <td>
                <input
                type = 'number'
                required= 'required'
                name='age'
                value ={editEmpData.age}
                placeholder='Enter age'
                onChange={handleEditChange}>
                </input>

            </td>
            <td>
                <button type= 'submit'>
                    <FaSave />
                    
                </button>
            </td>
        </tr>
    );
};

export default Editablerow;