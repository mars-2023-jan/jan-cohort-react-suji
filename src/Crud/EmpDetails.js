import React, { Fragment, useState } from "react";
import Employees from "./Employees";
import "./../Crud/emp.css";
import { nanoid } from "nanoid";
import Readonlyrow from "./Readonlyrow";
import Editablerow from "./Editablerow";

function EmpDetails(props) {
  const [employees, setEmployees] = useState(Employees);

  const [editEmpId, setEditEmpId] = useState(null);

  const [addFormData, setAddFormData] = useState({
    name: "",
    age: "",
  });

  const [editEmpData, setEditEmpData] = useState({
    name: "",
    age: "",
  });

  // const [deleteEmpData, setDeleteEmpData] = useState({
  //   name: "",
  //   age: "",
  // });

  const handleAddFormChange = (e) => {
    e.preventDefault();

    const fieldName = e.target.getAttribute("name");
    const fieldValue = e.target.value; //input element

    const newFormData = { ...addFormData };
    newFormData[fieldName] = fieldValue;
    setAddFormData(newFormData);

    console.log(addFormData);
  };

  const handleAddFormSubmit = (e) => {
    console.log("Adding EMP..");
    e.preventDefault();

    const newEmp = {
      id: nanoid(),
      name: addFormData.name,
      age: addFormData.age,
    };
    const newEmployees = [...employees, newEmp];
    setEmployees(newEmployees);
  };

  const handleEditClick = (e, emp) => {
    e.preventDefault();
    setEditEmpId(emp.id);

    const formValues = {
      name: emp.name,
      age: emp.age,
    };

    setEditEmpData(formValues);
  };

  const handleEditChange = (e) => {
    e.preventDefault();
    const fieldName = e.target.getAttribute("name");
    const fieldValue = e.target.value;

    const newFormData = { ...editEmpData };
    newFormData[fieldName] = fieldValue;

    setEditEmpData(newFormData);
  };

  const handleEditEmpSubmit = (e) => {
    e.preventDefault();

    const editedEmp = {
      id: editEmpId,
      name: editEmpData.name,
      age: editEmpData.age,
    };

    const newEmp = [...employees];
    const index = employees.findIndex((emp) => emp.id === editEmpId);
    newEmp[index] = editedEmp;
    setEmployees(newEmp);
    setEditEmpId(null);
  };

  const handleDelete = (e, empRow) => {
    e.preventDefault();
    const newEmp = [...employees];
    const index = employees.findIndex((emp) => emp.id === empRow.id);
    newEmp.splice(index, 1);
    setEmployees(newEmp);
  };

  return (
    <div>
      <form onSubmit={handleEditEmpSubmit}>
        <table>
          <thead>
            <tr>
              <th>NAME</th>
              <th>AGE</th>
              <th colSpan={2}>ACTION</th>
            </tr>
          </thead>
          <tbody>
            {employees.length > 0
              ? employees.map((emp) => {
                  return (
                    <Fragment>
                      {editEmpId === emp.id ? (
                        <Editablerow
                          editEmpData={editEmpData}
                          handleEditChange={handleEditChange}
                        />
                      ) : (
                        <Readonlyrow
                          emp={emp}
                          handleEditClick={handleEditClick}
                          handleDelete={handleDelete}
                        />
                      )}
                    </Fragment>
                  );
                })
              : "No Data Found"}
          </tbody>
        </table>
      </form>
      <br />

      {/* //   <tr key={emp.id}>
              //   //     <td>{emp.name}</td>
              //   //     <td>{emp.age}</td> 
              //   //     <td>
              //   //       <Link to="/edit"></Link>
              //   //       <button
              //   //         onClick={() => handleEdit(emp.id, emp.name, emp.age)}
              //   //       >
              //   //         Edit
              //   //       </button>
              //   //     </td>
              //   //     <td>
              //   //       <button onClick={() => handleDelete(emp.id)}>
              //   //         Delete
              //   //       </button>
              //   //     </td>
              //   //   </tr>
              //   // );
              // }) */}

      {/* <button onClick={handleadd}>ADD EMPLOYEE</button> */}
      <h2>Add Employee</h2>

      <form onSubmit={handleAddFormSubmit}>
        <input
          type="text"
          name="name"
          required="required"
          placeholder="Enter Name"
          onChange={handleAddFormChange}
        ></input>
        <input
          type="number"
          name="age"
          required="required"
          placeholder="Enter age"
          onChange={handleAddFormChange}
        ></input>
        <button type="submit">Add Employee</button>
      </form>
    </div>
  );
}

export default EmpDetails;
