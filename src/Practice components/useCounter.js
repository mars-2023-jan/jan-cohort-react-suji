import React, { useState, useEffect } from 'react';               //CUSTOM HOOK


function useCounter(initializer, componentname) {
    const [counter, setCounter] = useState(initializer)

    function resetCounter(){
        setCounter(counter+1)
    }

    useEffect(()=> {
        console.log('Button of '+componentname+ ' is clicked '+counter+' times');
    },[counter,componentname])
    return (
        resetCounter
    );
}

export default useCounter;