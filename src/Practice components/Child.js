import React from 'react';

function Child(childtomain) {
    const data = 'this data is passed from child component to the main component'
    return (
        <div>
            <button onClick={() => childtomain(data)}>Click(child)</button>
        </div>
    );
}

export default Child;