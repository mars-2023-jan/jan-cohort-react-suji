import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Dynamiccomponent from './Dynamiccomponent';  

function Parent(props) {
    const [user, setUser]= useState("usera")
    const navigate = useNavigate();

    function handleClick(){
        navigate('/');
    }
    return (
        <div>
            <Dynamiccomponent user={user} />
            <button onClick={()=> setUser("usera")}> Switch to UserA</button><br></br>
            <button onClick={()=> setUser("userb")}> Switch to UserB</button><br></br>
            <button onClick={handleClick}>Go Home</button>
        </div>
    );
}

export default Parent;