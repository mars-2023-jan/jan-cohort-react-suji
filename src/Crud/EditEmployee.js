import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Employees from "./Employees";

function EditEmployee(props) {
  const [name, setName] = useState("");
  const [id, setId] = useState("");
  const [age, setAge] = useState("");

    let index= Employees.findIndex(val=> val.id==id)
    let navigate = useNavigate()


  const handleClick = (e)=>{
    // console.log('EDITING..')
    // console.log(index);
    e.preventDefault();
    let emp = Employees[index];
    emp.name = name;
    emp.age = age;
    emp.id = id;
    navigate('/')
  }

  useEffect(() => {
    setName(localStorage.getItem("name"));
    setAge(localStorage.getItem("age"));
    setId(localStorage.getItem("id"));
  },[]);

  return (
    <div>
      <form>
        <input
          type="text"
          placeholder="name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        /><br></br>
        <input
          type="number"
          placeholder="age"
          value={age}
          onChange={(e) => setAge(e.target.value)}
        /> <br></br>
      </form>
      <br></br>
      <button type= 'submit' onClick={(e)=> handleClick(e)}>UPDATE</button>
    </div>
  );
}

export default EditEmployee;
