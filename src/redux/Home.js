import React from "react";
import { useSelector } from "react-redux";
import { increment, decrement } from "./Action";
import { useDispatch } from "react-redux";
import { getData } from "./Action";
import Table from "react-bootstrap/Table";
import { useEffect } from "react";

function Home(props) {
  console.log("State getting Accessed");
  const state = useSelector((state) => state);
  const dispatch = useDispatch();
  console.log(state.tableData);
  //   console.log(data);

  //   useEffect(() => {
  //     dispatch(getData());
  //   }, [dispatch]);

  //   const evenIds = data.filter((item) => item.id % 2 === 0);
  return (
    <div className="App">
      <h2>Counter : {state.count.counter}</h2>
      <button onClick={() => dispatch(increment())}>ADD</button>
      <button onClick={() => dispatch(decrement())}>SUBTRACT</button>
      <hr />
      <input
        type="radio"
        name="color"
        value="red"
        onClick={(e) =>
          dispatch({ type: "CHANGE COLOR", payload: e.target.value })
        }
      />{" "}
      RED&nbsp;
      <input
        type="radio"
        name="color"
        value="blue"
        onClick={(e) =>
          dispatch({ type: "CHANGE COLOR", payload: e.target.value })
        }
      />{" "}
      BLUE&nbsp;
      <input
        type="radio"
        name="color"
        value="green"
        onClick={(e) =>
          dispatch({ type: "CHANGE COLOR", payload: e.target.value })
        }
      />{" "}
      GREEN&nbsp;
      <p> Color : {state.favColor.color}</p>
      <hr />
      <button onClick={() => dispatch(getData())}>GET DATA</button>
      <br />
      <br />
      <>
        <table>
          <thead>
            <tr>
              <th>USER ID</th>
              <th>ID</th>
              <th>TITLE</th>
              <th>BODY</th>
            </tr>
          </thead>
          <tbody>
            {state.tableData
              .filter((item) => item.id % 2 === 0)
              .map((item) => (
                <tr key={item.id}>
                  <td>{item.userId}</td>
                  <td>{item.id}</td>
                  <td>{item.title}</td>
                  <td>{item.body}</td>
                </tr>
              ))}
          </tbody>
        </table>
      </>
    </div>
  );
}

export default Home;
