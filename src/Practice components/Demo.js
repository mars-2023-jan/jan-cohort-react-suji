import React from 'react';

function Demo(props) {
  return (
    <div>
      <h1>Hello World!!</h1>
      <p data-testid = "testid"> This is a test paragraph!</p>
    <div>
      <ul>
        <li>Red</li>
        <li>GREEN</li>
        <li>White</li>
        <li>Blue</li>
        <li>Grey</li>

      </ul>

    </div>

    </div>
  );
}

export default Demo;