import React, { useState } from "react";
import { UserContext } from "react";

function component1(props) {
  const [user, setuser] = "David Miller";

  return (
    <UserContext.Provider value={user}>
      <h2> component1</h2>
      <h2>hello{user}</h2>
      <component2 user={user} />
    </UserContext.Provider>
  );
}
export default component1;
