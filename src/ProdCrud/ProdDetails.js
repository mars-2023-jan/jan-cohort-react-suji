import React, { Component } from "react";
import ProdAction from "./ProdAction";

class ProdDetails extends Component {
  constructor() {
    super();
    this.state = {
      products: [],
    };
  }

  componentDidMount() {
    ProdAction.getProdData().then((res) => {
      this.setState({ products: res.data });
    });

    // deleteProd(){
    //   ProdAction.deleteProd(id).then( res => {
    //       this.setState({products: this.state.products.filter(product => product.id !== id)});
    //   });
    //}
  }

  render() {
    return (
      <div>
        <h2>Product Details</h2>
        <div className="row">
          <table>
            <tbody>
              <tr>
                <th>Product ID</th>
                <th>Product Name</th>
                <th>Product Description</th>
                <th>Price</th>
              </tr>
            </tbody>
            <tbody>
              {this.state.products.map((products) => (
                <tr key={products.id}>
                  <td>{products.productId}</td>
                  <td>{products.productName}</td>
                  <td>{products.productDesc}</td>
                  <td>{products.price}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <h2>Add Product</h2>

        <form>
          <input
            type="text"
            name="name"
            required="required"
            placeholder="Enter Product Name"
          ></input>
          <br></br>
          <br></br>
          <input
            type="text"
            name="description"
            required="required"
            placeholder="Enter Product Description"
          ></input>
          <br></br>
          <br></br>
          <input
            type="number"
            name="price"
            required="required"
            placeholder="Enter Product Price"
          ></input>
          <br></br>
          <br></br>

          <button type="submit">Add</button>
        </form>
      </div>
    );
  }
}

export default ProdDetails;
