import React, {Component} from "react"; //named export

class Statedemo extends Component{
     constructor(props){           //1st phase
        super(props);
        this.state={
            color: 'blue',
            count:0
        }
        this.updatestate = this.updatestate.bind(this)  //bind 
    }

    // static getDerivedStateFromProps(props){
    //     return{color:props.favcolor}
    // }

    updatestate(){
         this.setState({
            color:'green'
         })
    }

    componentDidMount(){
        setTimeout(() => {
            this.setstate({color: "Gray"})
            
        }, 2000);
    }

    render(){
        
        // const Styledbutton=  styled.button
        return(
            <div>
                {/* <p style={{styles.Header}}>this.state.color</p> */}
                <p>{this.state.color}</p>
                <p>{this.state.count}</p>
                <button></button>
            </div>
        );
    }
}
export default Statedemo;

//states on Class component