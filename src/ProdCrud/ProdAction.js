import axios from "axios";

const PRODUCT_API_BASE_URL = "http://localhost:8080/api";

class ProdAction {
  getProdData() {
    return axios.get(PRODUCT_API_BASE_URL + "/products");
  }

  createProd(productId, productName, productDesc,price) {
    return axios.post(PRODUCT_API_BASE_URL + "/product");
  }

  deleteProd(productId){
    return axios.delete(PRODUCT_API_BASE_URL + '/product'+ productId );
}
}

export default new ProdAction();
