

const tableDataReducer = (state = [], action) => {
  // console.log(state)
    switch (action.type) {
      case 'FETCH_TABLE_DATA':
        return action.payload;
      default:
        return state;
    }
  };

 export default tableDataReducer;