import React, { useState } from 'react';

function ExampleComponent() {
  const [items, setItems] = useState([]);

  const handleAdd = (newItem) => {
    setItems([...items, newItem]);
  };

  const handleUpdate = (index, updatedItem) => {
    const newItems = [...items];
    newItems[index] = updatedItem;
    setItems(newItems);
  };

  const handleDelete = (index) => {
    const newItems = [...items];
    newItems.splice(index, 1);
    setItems(newItems);
  };

  return (
    <div>
      <h1>My Items</h1>
      <ul>
        {items.map((item, index) => (
          <li key={index}>
            {item}
            <button onClick={() => handleDelete(index)}>Delete</button>
          </li>
        ))}
      </ul>
      <form onSubmit={(event) => {
          event.preventDefault();
          handleAdd(event.target.newItem.value);
          event.target.reset();
        }}>
        <label htmlFor="newItem">New Item:</label>
        <input type="text" name="newItem" />
        <button type="submit">Add</button>
      </form>
    </div>
  );
}

export default ExampleComponent;