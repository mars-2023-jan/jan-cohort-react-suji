const initialState = {
    color : ''
}

const colorReducer = (state =initialState, action)=>{
    console.log('Color reducer called...')
    const newState={...state}
    switch (action.type){
        case 'CHANGE COLOR':
            newState.color = action.payload;
                return newState;
        default:
            return state

}
}
export default colorReducer;