import { useNavigate } from "react-router-dom";
import React, { useState } from "react";
import Employees from "./Employees";

function AddEmployee() {
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  let navigate = useNavigate();
  
  const handleAdd = (e) => {
    let employee = {};
    employee.name = name;
    employee.age = age;
    employee.id = Employees.length + 1;
    Employees.push(employee);
    console.log(employee);
    navigate("/");
  };

  return (
    <div>
      <form>
        <label>NAME : </label>
        <input
          type="text"
          placeholder="Enter the Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        ></input>
        <label> AGE : </label>
        <input
          type="number"
          placeholder="Enter the Age"
          value={age}
          onChange={(e) => setAge(e.target.value)}
        ></input>
      </form>
      <br></br>
      <button type="submit" onClick={handleAdd}>
        ADD
      </button>
    </div>
  );
}

export default AddEmployee;
