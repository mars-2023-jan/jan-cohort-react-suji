import axios from "axios";

export const increment = () => {
  console.log("Action called");
  return {
    type: "INCREMENT",
  };
};

export const decrement = () => {
  return {
    type: "DECREMENT",
  };
};

export const getData = () => {
  return async (dispatch) => {
    const response = await axios.get(
      "http://localhost:8080/api/SortedProducts"
    );

    // console.log(response.data)
    dispatch({
      type: "FETCH_TABLE_DATA",
      payload: response.data,
    });
  };
};
