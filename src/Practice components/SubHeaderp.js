import React from "react";

class SubHeaderp extends React.Component{
    constructor(){
        super();
        this.state={
        name: "Pete",
        id : 101
        }
    }
    render(){
        <div>
        <h3>This is a Subheader written as a Class component</h3>
        <p> Scope: {this.props.scope}</p>
        <p>
            name in subheader is: {this.state.name}
        </p>
        </div>
    }
}
export default SubHeaderp;