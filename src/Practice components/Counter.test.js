import React from "react";
import { fireEvent, render, screen } from '@testing-library/react'
import Counter from "./Counter"

describe('Testing counter component', ()=>{
    
    test('counter is incremented on Add button click', ()=>{
        render(<Counter />)
        screen.debug();
        const counter = screen.getByTestId('counter');
        // const incrementBtn = screen.getByText('Add');

        // userEvent.click(incrementBtn)
        // userEvent.click(incrementBtn)
        // userEvent.click(incrementBtn)

        fireEvent.click(screen.getByText('Add'))
        fireEvent.click(screen.getByText('Add'))
        fireEvent.click(screen.getByText('Add'))
        fireEvent.click(screen.getByText('Add')) 

        expect(counter.textContent).toBe("4")

    });

    test('counter is decremented on Subtract button click', ()=>{
        render(<Counter />)
    const counter = screen.getByTestId('counter');  

        fireEvent.click(screen.getByText('subtract'))
        fireEvent.click(screen.getByText('subtract'))
        
        expect(counter.textContent).toBe("-2")
    });
})