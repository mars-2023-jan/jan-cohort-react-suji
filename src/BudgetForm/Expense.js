import { useState } from "react";

function Expense() {
  const [expense, setExpense] = useState({
    balance: "",
    childcare: "",
    insurance: "",
    mortage: "",
    grocery: "",
    budget: "",
  });

  const updateChange = (e) => {
    setExpense({ ...expense, [e.target.name]: e.target.value });
  };

  return (
    <div className="exp-container">
      <form>
        <label> Total Monthly Budget: </label>
        <input
          type="number"
          id="monthlybudget"
          name="budget"
          value={expense.budget}
          onChange={updateChange}
        ></input>
        <label> Monthly Balance: {(expense.budget)-(expense.childcare)-(expense.insurance)-(expense.mortage)-(expense.grocery)} </label>
      </form>

      <h2>Expenses</h2>

      <form>
        <label> Child Care : </label>
        <input
          type="number"
          id="childcare"
          name="childcare"
          value={expense.childcare}
          onChange={updateChange}
        ></input>
        <br></br>
        <br></br>
        <label> Insurance : </label>
        <input
          type="number"
          id="insurance"
          name="insurance"
          value={expense.insurance}
          onChange={updateChange}
        ></input>
        <br></br>
        <br></br>
        <label> Mortage : </label>
        <input
          type="number"
          id="mortage"
          name="mortage"
          value={expense.mortage}
          onChange={updateChange}
        ></input>
        <br></br>
        <br></br>
        <label> Grocery : </label>
        <input
          type="number"
          id="grocery"
          name="grocery"
          value={expense.grocery}
          onChange={updateChange}
        ></input>
        <br></br>
        <br></br>
      </form>
    </div>
  );
}

export default Expense;
