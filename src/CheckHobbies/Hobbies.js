import React, { useState } from "react";

function Hobbies(props) {
  const [hobbies, setHobbies] = useState({});
  const [myHobbies, setMyHobbies] = useState("");

  const updateChange = (e) => {
    hobbies[e.target.name] = !hobbies[e.target.name];
    let hobbiesList = "";
    for (let hobby in hobbies) {
      if (hobbies[hobby]) {
        hobbiesList += hobby;
        hobbiesList += ", ";
      }
    }
    setMyHobbies(hobbiesList.replace(/,\s*$/, ""));
  };

  return (
    <div>
      <input
        type="checkbox"
        id="sports"
        name="Sports"
        onChange={updateChange}
      />
      <label>Sports</label>
      <br />
      <input type="checkbox" id="music" name="Music" onChange={updateChange} />
      <label>Music</label>
      <br />
      <input
        type="checkbox"
        id="travel"
        name="Travel"
        onChange={updateChange}
      />
      <label>Travel</label>
      <br />
      <input
        type="checkbox"
        id="reading"
        name="Reading"
        onChange={updateChange}
      />
      <label>Reading</label>
      <br />

      <h2>My Hobbies includes: {myHobbies}</h2>
    </div>
  );
}

export default Hobbies;
