import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Form from "./Form";
import Output from "./Output";
import ErrorPage from "./ErrorPage";

function RouteForm(props) {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="*" element={<Form />} />   //used the wild card bcz the landing URL is different than root URL
          <Route path="/" element={<Form />} />
          <Route path="/form" element={<Form />} />
          <Route path="/output" element={<Output />} />
          <Route path="/errorpage" element={<ErrorPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default RouteForm;
