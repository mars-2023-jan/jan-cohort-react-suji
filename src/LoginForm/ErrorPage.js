import React from 'react';
import { useNavigate } from "react-router-dom";

function ErrorPage(props) {
    let navigate = useNavigate();
    return (
        <div id = "error">
            <h2>Sorry!! Invalid Credentials.</h2>
            <button
        onClick={() => {
          navigate("/");
        }}
      >
        Go Back
      </button>
        </div>
    );
}

export default ErrorPage;