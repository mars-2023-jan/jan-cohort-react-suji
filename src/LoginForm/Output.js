import React from "react";
import { useNavigate } from "react-router-dom";

function Output(props) {
  let navigate = useNavigate();
  return (
    <div id="output">
      <h2>Hello Sujitha, You are Successfully logged in!!</h2>
      <button
        onClick={() => {
          navigate("/");
        }}
      >
        Go Back
      </button>
    </div>
  );
}

export default Output;
