import React from "react";
import { FaEdit } from "react-icons/fa";
import { AiFillDelete } from "react-icons/ai";

const Readonlyrow = ({ emp, handleEditClick, handleDelete }) => {
  return (
    <tr key={emp.id}>
      <td> {emp.name}</td>
      <td> {emp.age}</td>
      <td>
        <button
          type="button"
          onClick={(e) => {
            handleEditClick(e, emp);
          }}
        >
          <FaEdit />
        </button>
        <button
          type="button"
          onClick={(e) => {
            handleDelete(e, emp);
          }}
        >
          <AiFillDelete />
        </button>
      </td>
    </tr>
  );
};

export default Readonlyrow;
