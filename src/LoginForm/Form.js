import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
function Form(props) {
  let navigate = useNavigate();
  const [formValues, setFormValues] = useState({
    username: "",
    email: "",
    password: "",
  });

  function handleChange(e) {
    const { name, value } = e.target;
    setFormValues((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  }

  function handleSubmit(event) {
    // event.preventDefault();
    if (
      formValues.username !== "sujitha92" ||
      formValues.email !== "sujitha@example.com" ||
      formValues.password !== "password123"
    ) {
      navigate("/errorpage");
    } else {
      navigate("/output");
    }
  }

  return (
    <div>
      <h1>Sign In Here!!</h1>
      <label>UserName : </label>
      <input
        type="text"
        placeholder="Enter your UserName"
        name="username"
        value={formValues.username}
        onChange={handleChange}
      />
      <br></br>
      <label>Password : </label>
      <input
        type="password"
        placeholder="Enter your Password"
        name="password"
        value={formValues.password}
        onChange={handleChange}
      />
      <br></br>
      <label>Email : </label>
      <input
        type="email"
        placeholder="Enter your Email"
        name="email"
        value={formValues.email}
        onChange={handleChange}
      />
      <br />
      <br />
      <button id="submit" onClick={handleSubmit}>
        Sign In
      </button>{" "}
      <br />
    </div>
  );
}

export default Form;
